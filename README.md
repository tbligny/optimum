# Install

## 0 GIT LFS
**Note**: Google efficientdet's models are large and are therefore stored using Git LFS. Don't forget to fetch those files after checking out the repo (I'm using Sourcetree for that matter: `Repository > Git LFS > Fetch LFS Content`).

## 1a. Windows
**Note**: As I'm mainly coding on my macOS laptop and deploying code on Linux servers, I haven't had the chance to test my code on Windows. There are probably broken things here and there but the overall logic should be common across platforms. Anyway, I've included some steps for Windows for information purposes BUT be aware I haven't included a windows YAML file! Some Python packages such as OpenCV and others are known to be platform specific, so I could not just copy-paste my macOS file for Windows. Still, given the time I'm pretty sure I could make it work on Windows too.

1. Open a **PowerShell** command prompt
2. Create an Anaconda environment by running `dev/win/create-environment.ps1`
3. From the project's **root** folder, install `optimum` in the current virtual environment (otherwise local Python imports will not work) by running:
```shell
pip install -e .
```

## 1b. macOS
**Note**: macOS steps are similar to Linux ones

1. Open a bash command prompt (I'm personnaly using ZSH and Oh-My-Zsh, but to each his own =))
2. Create an Anaconda environment by running `bash dev/macos/create-environment.sh`
3. From the project's **root** folder, install `optimum` in the current virtual environment (otherwise local Python imports will not work) by running:
```bash
pip install -e .
```

## 2. Fetch submodules (to retrieve external submodules such as the ones from Google)
1. From the root folder, run `git submodule update --init --recursive`


## 3. Configure directories
1. Create a log directory using `mkdir $HOME/logs` (I haven't had the time to put proper logs in my code though)


## Handling the Anaconda environment

**Note**: Scripts must be run from the project's *root* folder (because that's where the YAML file is)!

### Updating the Anaconda environment (`optimum`)

#### On Windows with Powershell:

```shell
.\dev\win\update-environment.ps1
```

#### On macOS/Linux using bash:

```bash
./dev/macos/update-environment.sh
```

### To export the virtual environment and save it in an `environment.yaml` file

#### On Windows with Powershell:

```shell
.\dev\win\export-environment.ps1
```

#### On macOS/Linux using bash:

```bash
./dev/macos/export-environment.sh
```

The environment will be saved in the `environment.yaml` file.

# Generate documentation using Sphinx

From the **root** folder:
```bash
sphinx-build -b html docs/source docs/output
```

![Doc](assets/doc_example.png)

# Usage example

From the **root** folder:

## Running tests

```bash
python -m unittest discover -v -s ./tests
```

![Tests](assets/tests.png)

## Image perspective transform

```bash
python social_distancing/tools/test_perspective_transform.py -i configs/cam_cote_vertu/video_frame.png
```
![Before](configs/cam_cote_vertu/video_frame.png)
![After](assets/capture_fixedperspective.jpg)

## Generate a homography matrix from an angle view image and a bird's view image

```bash
python social_distancing/tools/calibrate_from_orthophoto.py -i "configs/cam_cote_vertu/video_frame.png" -w "configs/cam_cote_vertu/world_image.jpg" -n 4 -u 0.0625 -o "configs/cam_cote_vertu/config.JSON" --name "CAM COTE VERTU"
```

Calibration:
![Calibration](configs/cam_cote_vertu/points.png)

JSON config output:
```json
{
    "name": "TER Côte-Vertu CAM XXX",
    "ID": "XXX-XXX-XXX",
    "homography": [
        [
            27.74827650517638,
            -9.148069233491922,
            310.5260686280531
        ],
        [
            -1.6753043346295375,
            1.7966192594280104,
            43.19518973548807
        ],
        [
            0.0017747936890496988,
            -0.018249936698207674,
            1.0
        ]
    ]
}
```

## Measure social distancing between individuals from camera footage

```bash
python social_distancing/inference/inference.py --model-id 0 --video-input "assets/cam_cote_vertu.mp4" --camera "configs/cam_cote_vertu/config.JSON" --display --output "." --start-frame 450
```

Click on image to see Youtube video:
[![Youtube video link](configs/cam_cote_vertu/social_distancing.png)](https://youtu.be/Tbrzr_yCO-U)


# Short description of the social_distancing packages 
**Note**: usually this part would not exist as it's supposed to be in the auto-generated Sphinx documentation but as my documentation is not perfect [it is just for demo purposes after all], it is still worth having a quick peak here.
## automl

Google library for recognizing objects through efficientdet models.

## camera

`configuration.py`: load/store a camera's JSON config (homography matrix).

`homography.py`: performing homographies

## inference

`analyzer.py`: filter through individuals in an image and compute distances between them

`inference.py`: social distancing logic

## tools

`calibrate_from_orthophoto.py`: calibration tool to compute a homography config

`test_perspective_transform.py`: perform a 4-point perspective transform

## utils

`opencv.py`: OpenCV tools to handle images/videos

`reduce_framerate.py`: reduce video framerate to ease compute usage

`visualization.py`: visualization tools (image conversion and such)

## tests

`test_homography.py`: homography unit test