import unittest
import numpy as np
import cv2

from social_distancing.camera.homography import (
    project_image_coordinates_to_world_coordinates,
    project_world_coordinates_to_image_coordinates,
)

class TestHomography(unittest.TestCase):
    """Homography testing.
    """

    def test_projection_cycle(self):
        """Tests a projection cycle:
            (1) Coordinates on camera image (IMAGE) -> Coordinates on satellite image (WORLD) -> Coordinates on camera image (IMAGE)
            (2) Coordinates on satellite image (WORLD) -> Coordinates on camera image (IMAGE) -> Coordinates on satellite image (WORLD)

            For each cycle, inputs and outputs must be the same (give or take, as there's a numerical precision loss)
        """
        # Some test points to compute a homography.
        world_points = np.array(
            [
                [1.9, 2], 
                [3.1, 5.6], 
                [5.6, 61.1], 
                [6.3, 1.2], 
                [0.2, 0.4], 
                [19.3, 39.5]
            ],
            dtype="float32",
        )

        image_points = np.array(
            [
                [725.9, 98.0],
                [32.9, 545.98],
                [178.9, 31.1],
                [98.4, 0.9],
                [15.6, 98.3],
                [400.2, 189.1],
            ],
            dtype="float32",
        )

        homography, _ = cv2.findHomography(world_points, image_points)
        # The homography should be a 3x3 matrix.
        self.assertEqual(homography.shape, (3, 3))

        # Let's perform the following projection: IMAGE -> WORLD -> IMAGE.
        projected_image_points = project_image_coordinates_to_world_coordinates(
            image_points, homography
        )
        backprojected_image_points = project_world_coordinates_to_image_coordinates(
            projected_image_points, homography
        )

        # Perform both projections should give us original points (give or take)
        np.testing.assert_allclose(image_points, backprojected_image_points)

        # Now let's do it in reverse order: WORLD -> IMAGE -> WORLD.
        projected_world_points = project_world_coordinates_to_image_coordinates(
            world_points, homography
        )
        backprojected_world_points = project_image_coordinates_to_world_coordinates(
            projected_world_points, homography
        )

        np.testing.assert_allclose(world_points, backprojected_world_points)

if __name__ == "__main__":
    unittest.main()
