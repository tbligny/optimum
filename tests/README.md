## Unit tests

To launch unit tests, execute the following command from the project's root directory:
```
python -m unittest discover -v -s ./tests
```