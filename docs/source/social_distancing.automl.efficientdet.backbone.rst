social\_distancing.automl.efficientdet.backbone package
=======================================================

Submodules
----------

social\_distancing.automl.efficientdet.backbone.backbone\_factory module
------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.backbone.backbone_factory
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.backbone.efficientnet\_builder module
----------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.backbone.efficientnet_builder
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.backbone.efficientnet\_builder\_test module
----------------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.backbone.efficientnet_builder_test
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.backbone.efficientnet\_lite\_builder module
----------------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.backbone.efficientnet_lite_builder
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.backbone.efficientnet\_lite\_builder\_test module
----------------------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.backbone.efficientnet_lite_builder_test
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.backbone.efficientnet\_model module
--------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.backbone.efficientnet_model
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.backbone.efficientnet\_model\_test module
--------------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.backbone.efficientnet_model_test
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: social_distancing.automl.efficientdet.backbone
   :members:
   :undoc-members:
   :show-inheritance:
