social\_distancing.automl package
=================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   social_distancing.automl.efficientdet

Module contents
---------------

.. automodule:: social_distancing.automl
   :members:
   :undoc-members:
   :show-inheritance:
