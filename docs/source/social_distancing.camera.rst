social\_distancing.camera package
=================================

Submodules
----------

social\_distancing.camera.configuration module
----------------------------------------------

.. automodule:: social_distancing.camera.configuration
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.camera.homography module
-------------------------------------------

.. automodule:: social_distancing.camera.homography
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: social_distancing.camera
   :members:
   :undoc-members:
   :show-inheritance:
