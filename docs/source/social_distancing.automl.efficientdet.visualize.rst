social\_distancing.automl.efficientdet.visualize package
========================================================

Submodules
----------

social\_distancing.automl.efficientdet.visualize.shape\_utils module
--------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.visualize.shape_utils
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.visualize.standard\_fields module
------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.visualize.standard_fields
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.visualize.static\_shape module
---------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.visualize.static_shape
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.visualize.vis\_utils module
------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.visualize.vis_utils
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.visualize.vis\_utils\_test module
------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.visualize.vis_utils_test
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: social_distancing.automl.efficientdet.visualize
   :members:
   :undoc-members:
   :show-inheritance:
