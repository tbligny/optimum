social\_distancing.utils package
================================

Submodules
----------

social\_distancing.utils.opencv module
--------------------------------------

.. automodule:: social_distancing.utils.opencv
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.utils.reduce\_framerate module
-------------------------------------------------

.. automodule:: social_distancing.utils.reduce_framerate
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.utils.visualization module
---------------------------------------------

.. automodule:: social_distancing.utils.visualization
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: social_distancing.utils
   :members:
   :undoc-members:
   :show-inheritance:
