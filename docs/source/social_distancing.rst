social\_distancing package
==========================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   social_distancing.automl
   social_distancing.calibrate
   social_distancing.camera
   social_distancing.inference
   social_distancing.utils

Module contents
---------------

.. automodule:: social_distancing
   :members:
   :undoc-members:
   :show-inheritance:
