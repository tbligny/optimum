social\_distancing.automl.efficientdet package
==============================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   social_distancing.automl.efficientdet.aug
   social_distancing.automl.efficientdet.backbone
   social_distancing.automl.efficientdet.dataset
   social_distancing.automl.efficientdet.keras
   social_distancing.automl.efficientdet.object_detection
   social_distancing.automl.efficientdet.visualize

Submodules
----------

social\_distancing.automl.efficientdet.coco\_metric module
----------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.coco_metric
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.dataloader module
--------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.dataloader
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.dataloader\_test module
--------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.dataloader_test
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.det\_model\_fn module
------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.det_model_fn
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.det\_model\_fn\_test module
------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.det_model_fn_test
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.efficientdet\_arch module
----------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.efficientdet_arch
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.efficientdet\_arch\_test module
----------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.efficientdet_arch_test
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.hparams\_config module
-------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.hparams_config
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.hparams\_config\_test module
-------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.hparams_config_test
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.inference module
-------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.inference
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.iou\_utils module
--------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.iou_utils
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.iou\_utils\_test module
--------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.iou_utils_test
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.main module
--------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.main
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.model\_inspect module
------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.model_inspect
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.model\_inspect\_test module
------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.model_inspect_test
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.nms\_np module
-----------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.nms_np
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.tensorrt module
------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.tensorrt
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.utils module
---------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.utils
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.utils\_test module
---------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.utils_test
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: social_distancing.automl.efficientdet
   :members:
   :undoc-members:
   :show-inheritance:
