social\_distancing.automl.efficientdet.dataset package
======================================================

Submodules
----------

social\_distancing.automl.efficientdet.dataset.create\_coco\_tfrecord module
----------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.dataset.create_coco_tfrecord
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.dataset.create\_coco\_tfrecord\_test module
----------------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.dataset.create_coco_tfrecord_test
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.dataset.create\_pascal\_tfrecord module
------------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.dataset.create_pascal_tfrecord
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.dataset.create\_pascal\_tfrecord\_test module
------------------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.dataset.create_pascal_tfrecord_test
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.dataset.label\_map\_util module
----------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.dataset.label_map_util
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.dataset.tfrecord\_util module
--------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.dataset.tfrecord_util
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: social_distancing.automl.efficientdet.dataset
   :members:
   :undoc-members:
   :show-inheritance:
