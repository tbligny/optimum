social\_distancing.automl.efficientdet.object\_detection package
================================================================

Submodules
----------

social\_distancing.automl.efficientdet.object\_detection.argmax\_matcher module
-------------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.object_detection.argmax_matcher
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.object\_detection.box\_coder module
--------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.object_detection.box_coder
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.object\_detection.box\_list module
-------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.object_detection.box_list
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.object\_detection.faster\_rcnn\_box\_coder module
----------------------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.object_detection.faster_rcnn_box_coder
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.object\_detection.matcher module
-----------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.object_detection.matcher
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.object\_detection.preprocessor module
----------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.object_detection.preprocessor
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.object\_detection.region\_similarity\_calculator module
----------------------------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.object_detection.region_similarity_calculator
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.object\_detection.shape\_utils module
----------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.object_detection.shape_utils
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.object\_detection.target\_assigner module
--------------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.object_detection.target_assigner
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.object\_detection.tf\_example\_decoder module
------------------------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.object_detection.tf_example_decoder
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: social_distancing.automl.efficientdet.object_detection
   :members:
   :undoc-members:
   :show-inheritance:
