social\_distancing.automl.efficientdet.aug package
==================================================

Submodules
----------

social\_distancing.automl.efficientdet.aug.autoaugment module
-------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.aug.autoaugment
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.automl.efficientdet.aug.autoaugment\_test module
-------------------------------------------------------------------

.. automodule:: social_distancing.automl.efficientdet.aug.autoaugment_test
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: social_distancing.automl.efficientdet.aug
   :members:
   :undoc-members:
   :show-inheritance:
