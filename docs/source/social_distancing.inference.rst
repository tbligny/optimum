social\_distancing.inference package
====================================

Submodules
----------

social\_distancing.inference.analyzer module
--------------------------------------------

.. automodule:: social_distancing.inference.analyzer
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.inference.benchmark module
---------------------------------------------

.. automodule:: social_distancing.inference.benchmark
   :members:
   :undoc-members:
   :show-inheritance:

social\_distancing.inference.inference module
---------------------------------------------

.. automodule:: social_distancing.inference.inference
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: social_distancing.inference
   :members:
   :undoc-members:
   :show-inheritance:
