# The '--prune' option is used to uninstall libraries that aren't referenced in the YAML file.
conda env update --file .\environment_win.yaml --prune
