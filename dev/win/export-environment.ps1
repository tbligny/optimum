
# Export the environment to a YAML file

# By default PowerShell encodes files in UTF-16.
# It leads to issues because Anaconda expects UTF-8.
#
# The following command enables exporting the environment in ASCII,
# which solves the problem as ASCII is a sub-group within UTF-8.
conda env export --no-builds | Set-Content -Encoding Ascii -Path environment_win.yaml
