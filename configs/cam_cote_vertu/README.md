### Example command
```
python /Users/tim/git/exo_proj/optimum/social_distancing/tools/calibrate_from_orthophoto.py --image /Users/tim/git/exo_proj/optimum/configs/cam_cote_vertu/video_frame.png --world /Users/tim/git/exo_proj/optimum/configs/cam_cote_vertu/world_image.jpg --name "CAM COTE VERTU" --output /Users/tim/git/exo_proj/optimum/config.json --units-per-pixel 0.0625
```