# The logic from this file is greatly inspired from https://bitbucket.org/Nicolas/trafficintelligence/src/default/scripts/compute-homography.py

import sys
sys.path.append(".")
import os
import argparse
import enum
import types

import matplotlib.pyplot as plt
import matplotlib.lines
import numpy as np
import cv2

from social_distancing.utils.opencv import wait_for_window_closure
from social_distancing.camera.homography import (
    project_image_coordinates_to_world_coordinates,
    project_world_coordinates_to_image_coordinates,
)
from social_distancing.camera import CameraConfiguration


class State(enum.Enum):
    SELECTING_VIDEO_IMAGE_POINT = 1
    SELECTING_WORLD_IMAGE_POINT = 2


video_image = None
world_image = None

current_state = None
video_image_plot = None
world_image_plot = None

video_image_mpl_artists = []
world_image_mpl_artists = []

is_panning = False
pan_data = None

video_image_points = []
world_image_points = []

args = None


def _on_click(event):
    global current_state, video_image_plot, world_image_plot, video_image_mpl_artists, world_image_mpl_artists, is_panning, pan_data

    # on the mouse: left = 1, wheel = 2, right = 3
    if event.button == 1:
        if event.xdata is None or event.ydata is None:
            # The user did not click on a plot, they probably clicked in the white region.
            return

        # Only listen to mouse clicks on the plot according to our current state.
        good_plot = (
            video_image_plot
            if current_state == State.SELECTING_VIDEO_IMAGE_POINT
            else world_image_plot
        )
        if event.inaxes is not good_plot:
            return

        # Keep track of this point by adding it to a list.
        point = [event.xdata, event.ydata]
        good_list = (
            video_image_points
            if current_state == State.SELECTING_VIDEO_IMAGE_POINT
            else world_image_points
        )
        good_list.append(point)

        # Draw a cross at the position of the click.
        color = "r" if current_state == State.SELECTING_VIDEO_IMAGE_POINT else "c"
        line = matplotlib.lines.Line2D([event.xdata], [event.ydata], marker="x", color=color)
        good_plot.add_line(line)

        label = good_plot.text(
            event.xdata + 5,
            event.ydata + 5,
            s=len(good_list),
            color=color,
            clip_on=True,
            fontsize="medium",
        )

        history = (
            video_image_mpl_artists
            if current_state == State.SELECTING_VIDEO_IMAGE_POINT
            else world_image_mpl_artists
        )

        history.append((line, label))

        # Update the current state.
        if current_state == State.SELECTING_VIDEO_IMAGE_POINT:
            current_state = State.SELECTING_WORLD_IMAGE_POINT
        else:
            current_state = State.SELECTING_VIDEO_IMAGE_POINT

        # Update the info label.
        update_figure_suptitle()

        # Update the plot
        plt.gcf().canvas.draw()
    elif event.button == 3 and event.inaxes:
        try:
            is_panning = True

            pan_data = types.SimpleNamespace(
                lim=event.inaxes.viewLim.frozen(),
                trans=event.inaxes.transData.frozen(),
                trans_inverse=event.inaxes.transData.inverted().frozen(),
                bbox=event.inaxes.bbox.frozen(),
                x=event.x,
                y=event.y,
            )
        except ValueError:
            print("Could not get pan data.")


def _on_keyboard_event(event):
    global current_state

    if event.key == "d":
        if current_state != State.SELECTING_VIDEO_IMAGE_POINT:
            # We're waiting for the world point (can't quit now)
            return

        if len(video_image_points) < 4:
            print("Not enough points yet.")
            return

        assert len(video_image_points) == len(
            world_image_points
        ), "Should have same number of points in each image."

        plt.close(event.canvas.figure)

        compute_homography_and_output(video_image_points, world_image_points)
    elif event.key == "ctrl+z":
        # Undo the last point selection.
        if current_state == State.SELECTING_WORLD_IMAGE_POINT:
            if len(video_image_mpl_artists) < 1:
                return

            video_image_mpl_artists[-1][0].remove()
            video_image_mpl_artists[-1][1].remove()
            del video_image_mpl_artists[-1]
            del video_image_points[-1]

            current_state = State.SELECTING_VIDEO_IMAGE_POINT
        elif current_state == State.SELECTING_VIDEO_IMAGE_POINT:
            if len(world_image_mpl_artists) < 1:
                return

            world_image_mpl_artists[-1][0].remove()
            world_image_mpl_artists[-1][1].remove()
            del world_image_mpl_artists[-1]
            del world_image_points[-1]

            current_state = State.SELECTING_WORLD_IMAGE_POINT

        update_figure_suptitle()
        plt.gcf().canvas.draw()


def _on_mouse_wheel_scroll(event):
    # Get the position of the mouse.
    x = event.xdata
    y = event.ydata

    # If the user scrolled outside of the plot then we ignore this event.
    if x is None or y is None:
        return

    if event.button == "up":
        # Zoom in
        scale_factor = 0.7
    elif event.button == "down":
        # Zoom out
        scale_factor = 1.4
    else:
        # Something else (eg. middle mouse click) which does not interest us.
        return

    # Get the dimensions of the plot.
    limit_x = event.inaxes.get_xlim()
    limit_y = event.inaxes.get_ylim()

    # Calculate the distance between the cursor and the edge of the plot.
    x_left = x - limit_x[0]
    x_right = limit_x[1] - x
    y_top = y - limit_y[0]
    y_bottom = limit_y[1] - y

    event.inaxes.set_xlim([x - x_left * scale_factor, x + x_right * scale_factor])
    event.inaxes.set_ylim([y - y_top * scale_factor, y + y_bottom * scale_factor])

    # Redraw to display the new axes limits.
    plt.gcf().canvas.draw()


def _on_mouse_moved(event):
    global is_panning, pan_data

    if not is_panning:
        return

    if pan_data and event.inaxes:
        dx = event.x - pan_data.x
        dy = event.y - pan_data.y

        if dx == 0 and dy == 0:
            return

        result = pan_data.bbox.translated(-dx, -dy).transformed(pan_data.trans_inverse)

        valid = np.isfinite(result.transformed(pan_data.trans))
        points = result.get_points().astype(object)

        # Ignorons les valeurs invalides.
        points[~valid] = None

        event.inaxes.set_xlim(points[:, 0])
        event.inaxes.set_ylim(points[:, 1])

        plt.gcf().canvas.draw()


def _on_mouse_released(event):
    global is_panning, pan_data

    if event.button == 3:
        is_panning = False

        pan_data = None
        plt.gcf().canvas.toolbar.push_current()


def show_video_and_world_plots(video_img, world_img):
    global current_state, world_image_plot, video_image_plot

    figure = plt.figure()

    # Disable the default "q for quitting" key binding.
    del figure.canvas.callbacks.callbacks["key_press_event"]

    # Be notified when the user clicks on the image, hits a key, scrolls with the mouse or moves the mouse.
    figure.canvas.mpl_connect("button_press_event", _on_click)
    figure.canvas.mpl_connect("key_press_event", _on_keyboard_event)
    figure.canvas.mpl_connect("scroll_event", _on_mouse_wheel_scroll)
    figure.canvas.mpl_connect("motion_notify_event", _on_mouse_moved)
    figure.canvas.mpl_connect("button_release_event", _on_mouse_released)

    video_image_plot = figure.add_subplot(1, 2, 1)
    video_image_plot.imshow(video_img)
    video_image_plot.title.set_text("Video image")

    world_image_plot = figure.add_subplot(1, 2, 2)
    world_image_plot.imshow(world_img)
    world_image_plot.title.set_text("Orthophoto")

    current_state = State.SELECTING_VIDEO_IMAGE_POINT

    update_figure_suptitle()

    figure.canvas.toolbar.push_current()

    plt.tight_layout()
    plt.show()


def update_figure_suptitle():
    if current_state == State.SELECTING_VIDEO_IMAGE_POINT:
        length = len(video_image_points)
        text = "Select video image point #{0}".format(length + 1)
        if length >= 4:
            text += "\nPress 'd' if done"
        plt.gcf().suptitle(text)
    else:
        plt.gcf().suptitle("Select corresponding world point")


def compute_homography_and_output(video_points, world_points):
    global args, video_image, world_image
    world_image_pts = args.units_per_pixel * np.array(world_points)
    video_frame_pts = np.array(video_points)

    homography, _ = cv2.findHomography(world_image_pts, video_frame_pts)

    if homography.size > 0:
        config = CameraConfiguration(args.camera_name, homography)
        config.write_to_file(args.output_file)
    else:
        print("Could not compute homography.")
        return

    # Convert RGB to BGR format.
    world_image = cv2.cvtColor(world_image, cv2.COLOR_BGR2RGB)
    video_frame = cv2.cvtColor(video_image, cv2.COLOR_BGR2RGB)

    projected_world_image_pts = project_world_coordinates_to_image_coordinates(
        world_image_pts, homography
    )
    projected_video_frame_pts = project_image_coordinates_to_world_coordinates(
        video_frame_pts, homography
    )

    difference = projected_video_frame_pts - world_image_pts
    mean_relative_error = np.mean((np.abs(difference) / world_image_pts)) * 100
    print("Mean reprojection error: {:.2f} %".format(mean_relative_error))

    for i in range(world_image_pts.shape[0]):
        blue = (255, 0, 0)  # in BGR format
        red = (0, 0, 255)  # in BGR format

        # World image
        cv2.circle(
            world_image,
            tuple(np.int32(np.round(world_image_pts[i] / args.units_per_pixel))),
            2,
            blue,
        )
        cv2.circle(
            world_image,
            tuple(np.int32(np.round(projected_video_frame_pts[i] / args.units_per_pixel))),
            2,
            red,
        )
        cv2.putText(
            world_image,
            str(i + 1),
            tuple(np.int32(np.round(world_image_pts[i] / args.units_per_pixel)) + 5),
            cv2.FONT_HERSHEY_PLAIN,
            2.0,
            blue,
            2,
        )

        # Video frame
        cv2.circle(video_frame, tuple(np.int32(np.round(video_frame_pts[i]))), 2, blue)
        cv2.circle(video_frame, tuple(np.int32(np.round(projected_world_image_pts[i]))), 2, red)
        cv2.putText(
            video_frame,
            str(i + 1),
            tuple(np.int32(np.round(video_frame_pts[i]) + 5)),
            cv2.FONT_HERSHEY_PLAIN,
            2.0,
            blue,
            2,
        )

    cv2.imshow("video frame", video_frame)
    cv2.imshow("world image", world_image)

    # Close both windows when the "video frame" is closed.
    # This is not the best solution...
    wait_for_window_closure("video frame")


def main():
    global args, world_image, video_image
    parser = argparse.ArgumentParser(
        description="""The program computes the homography matrix from at least 4 non-colinear point correspondences in a video frame and a aerial photo/ground map
    If providing video and world images, with a number of points to input
    and a ratio to convert pixels to world distance unit (eg meters per pixel),
    the images will be shown side by side and the user should select corresponding
    points in the world and image images.""",
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )

    # There are two ways to specify input data: either using -i, -w, -o and -n (all four) or using -f. The first way it the manual approach
    # and the second method is the simpler method.

    parser.add_argument(
        "-i", "--image", dest="video_frame_filename", help="filename of the video frame"
    )
    parser.add_argument(
        "-w", "--world", dest="world_image_filename", help="filename of the aerial photo/ground map"
    )
    parser.add_argument("-o", "--output", dest="output_file", help="path to the output file")
    parser.add_argument(
        "-n", "--name", dest="camera_name", help="name of the camera being calibrated"
    )

    # This is the simpler method where we specify and input folder containing all the necessary info.
    parser.add_argument(
        "-f",
        "--folder",
        dest="input_folder",
        help="Path to input folder named `camera_name` containing `video_frame.png` and `world_image.jpg`. Output will be written in this folder with name `config.JSON`. If you use this option, you cannot use --image and --world as well.",
    )

    # The global options.
    parser.add_argument(
        "-u",
        "--units-per-pixel",
        dest="units_per_pixel",
        help="number of units per pixel",
        type=float,
        required=True,
    )

    args = parser.parse_args()

    if args.input_folder and (
        args.video_frame_filename
        or args.world_image_filename
        or args.output_file
        or args.camera_name
    ):
        parser.error("you cannot specify an input folder and also use the manual approach.")

    if args.input_folder is None and not (
        args.video_frame_filename
        and args.world_image_filename
        and args.output_file
        and args.camera_name
    ):
        parser.error("must specify -i, -w, -o and -n (all four) or -f.")

    if args.input_folder:
        assert os.path.isdir(args.input_folder), "input_folder must be a directory"

        args.camera_name = os.path.basename(args.input_folder)
        video_frame_filename = os.path.join(args.input_folder, "video_frame.png")
        world_image_filename = os.path.join(args.input_folder, "world_image.jpg")

        assert os.path.isfile(
            video_frame_filename
        ), "video_frame.png could not be found in input folder"
        assert os.path.isfile(
            world_image_filename
        ), "world_image.jpg could not be found in input folder"

        args.video_frame_filename = video_frame_filename
        args.world_image_filename = world_image_filename

        output_file = os.path.join(args.input_folder, "config.JSON")

        # If config.JSON already exists in input folder, ask the user if they want to overwrite the file.
        if os.path.isfile(output_file):
            while True:
                data = input("The config.JSON file already exists. Overwrite? [y/n]").lower()
                if data not in ["y", "n"]:
                    print("Incorrect response")
                    continue
                elif data == "y":
                    # We will overwrite. Keep going with the program.
                    break
                else:
                    print("Goodbye")
                    return

        args.output_file = output_file

    world_image = plt.imread(args.world_image_filename)
    video_image = plt.imread(args.video_frame_filename)

    show_video_and_world_plots(video_image, world_image)


if __name__ == "__main__":
    main()
