#!/usr/bin/env python3

import argparse

import matplotlib.pyplot as plt
import numpy as np

from social_distancing.camera.homography import (
    perform_perspective_transform,
    calculate_four_point_homography,
)


class PerspectiveViewer(object):
    def __init__(self, image, figure):
        self.image = image
        self.figure = figure

        # Notification when the user clicks on the image.
        figure.canvas.mpl_connect("button_press_event", self._on_click)

        self.original_image_plot = figure.add_subplot(2, 1, 1)
        self.warped_image_plot = figure.add_subplot(2, 1, 2)

        self.selected_coordinates = []

    def draw_grid(self, homography, original_coordinates, warped_coordinates):
        (tl, tr, _, bl) = warped_coordinates

        N = 5

        x_min = tl[0]
        x_max = tr[0]
        y_min = tl[1]
        y_max = bl[1]
        x = np.linspace(x_min, x_max, num=N)
        y = np.linspace(y_min, y_max, num=N)

        self.warped_image_plot.hlines(y, x_min, x_max, linestyle="dashed", color="blue")
        self.warped_image_plot.vlines(x, y_min, y_max, linestyle="dashed", color="blue")

        (tl, tr, _, bl) = original_coordinates
        left_line_slope = (bl[1] - tl[1]) / (bl[0] - tl[0])
        top_line_slope = (tr[1] - tl[1]) / (tr[0] - tl[0])

        step_size_left_line = (bl[0] - tl[0]) / N
        step_size_top_line = (tr[0] - tl[0]) / N

        def plot_line(pt1, pt2):
            self.original_image_plot.plot([pt1[0], pt2[0]], [pt1[1], pt2[1]])

        for i in range(1, N + 1):
            for j in range(1, N + 1):
                pt1 = [
                    tl[0] + i * step_size_left_line,
                    tl[1] + i * step_size_left_line * left_line_slope,
                ]
                pt2 = [
                    tl[0] + N * step_size_top_line + i * step_size_left_line,
                    tl[1]
                    + N * step_size_top_line * top_line_slope
                    + i * step_size_left_line * left_line_slope,
                ]

                # plot_line(pt1, pt2)

    def _on_click(self, event):
        if event.xdata is None or event.ydata is None:
            return

        # Only listens to mouse clicks over the original canvas
        if event.inaxes is not self.original_image_plot:
            return
        # Keep track of a point by adding it to a list
        point = [event.xdata, event.ydata]
        self.selected_coordinates.append(point)
        print(point)

        # Draw a circle at the click's position
        circle = plt.Circle(point, 30)
        self.original_image_plot.add_artist(circle)

        if len(self.selected_coordinates) == 4:
            # Apply a 4-point perspective transform to get a bird's view
            coordinates_np = np.array(self.selected_coordinates, dtype="float32")
            dst, homography = calculate_four_point_homography(coordinates_np)
            self.draw_grid(homography, self.selected_coordinates, dst)

            warped_image = perform_perspective_transform(image, homography)
            import cv2

            frame_normed = (
                255
                * (warped_image - warped_image.min())
                / (warped_image.max() - warped_image.min())
            )
            frame_normed = np.array(frame_normed, np.int)
            frame_normed = frame_normed[:, :, ::-1]
            cv2.imwrite(
                r"Capture_fixedperspective.jpg",
                frame_normed,
            )
            self.warped_image_plot.imshow(warped_image)

            # Clear the saved points.
            self.selected_coordinates = []

        # Update the plot
        self.figure.canvas.draw()

    def display(self):
        self.original_image_plot.imshow(image)
        plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Performs a perspective transform to get a bird's view."
    )
    parser.add_argument("-i", "--image", help="path to image file", required=True)
    args = parser.parse_args()

    image = plt.imread(args.image)
    figure = plt.figure()

    viewer = PerspectiveViewer(image, figure)
    viewer.display()
