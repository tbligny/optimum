import sys
sys.path.append(".")

import argparse
import collections
import os
import sys
import time

import numpy as np
import tqdm
import cv2

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"  # Disable Tensorflow info messages

from social_distancing.automl.efficientdet import hparams_config, inference, utils
from social_distancing.camera import CameraConfiguration
from social_distancing.utils.opencv import (
    video_frame_generator,
    resize_image,
    create_background_subtractor,
)
from social_distancing.inference.analyzer import (
    FrameData,
    FrameDataAggregator,
)
from social_distancing.utils.visualization import draw_frame_data_on_image

from datetime import datetime

SAVED_MODEL_DIR = "models/efficientdet-d{0}-sv-batch{1}"
RECENT_FRAMES_PEOPLE_COUNT_HISTORY = 5


def create_inference_driver(model_id, saved_model_path, min_score_thresh, batch_size):
    """Creates an instance of efficientdet.inference.ServingDriver and loads the TF model with it.

    Args:
        model_id (int): the model id, e.g. 0 if "efficientdet-d0", 1 if "efficientdet-d1", etc.
        saved_model_path (str): path the model containing the model in SavedModel format.
        min_score_thresh (float): the minimum score threshold below
                                    which detections are not drawn.
        batch_size (int): inference batch size.
    """
    assert batch_size > 0, "batch size must be greater than 0."

    model_name = "efficientdet-d{0}".format(model_id)

    model_config = hparams_config.get_detection_config(model_name)
    # model_config.override(hparams)  # Add custom overrides
    model_config.image_size = utils.parse_image_size(model_config.image_size)

    inference_driver = inference.ServingDriver(
        model_name=model_name,
        ckpt_path=None,
        min_score_thresh=min_score_thresh,
        batch_size=batch_size,
        use_xla=False,
        model_params=model_config.as_dict(),
    )

    start_time = time.perf_counter()
    # Load the model in the "SavedModel" format.
    inference_driver.load(saved_model_path)
    print("Loaded model in {:.2f}s.".format(time.perf_counter() - start_time))

    return inference_driver


def should_process_video_frame(
    recent_frames_people_count, background_subtractor, video_frame, video_frame_index
):
    """Aims to predict whether or not a particular video frame would be relevant to perform inference on.
       This takes a list of people count in the last few frames and also uses background supression techniques (for movement detection)
       to be able to give on a verdict on whether or not a particular frame should be analyzed.

    Args:
        recent_frames_people_count (list): the list of people in the last frames. Last entry is most recent frame.
        background_subtractor (bgsegm_BackgroundSubtractorMOG): the background supressor instance.
        video_frame (np.ndarray): the video frame to perform the analysis on.
        video_frame_index (int): the index of the video frame being processed.
    Returns:
        Boolean: Whether or not inference should be performed on this video frame.
    """

    # Regardless of whether we will use it or not, ALWAYS update the background subtractor
    # so that it can learn the background.
    foreground_mask = background_subtractor.apply(video_frame)

    if len(recent_frames_people_count) < RECENT_FRAMES_PEOPLE_COUNT_HISTORY:
        # We don't have enough frames of data yet, so we assume that there are people
        # to avoid skipping valuable video frames.
        people_in_the_last_frames = True
    else:
        # We have more than RECENT_FRAMES_PEOPLE_COUNT_HISTORY processed frames.
        # Check whether or not at least a single person was detected in these last frames.
        people_in_the_last_frames = sum(recent_frames_people_count) > 0

    if people_in_the_last_frames:
        # If there were people in the recent frames then we should analyze this frame.
        # Otherwise, we proceed with the motion detection.
        return True

    if video_frame_index < 30 + 1:
        # The background subtractor needs to see some video frames in order to learn the background of the image.
        # For the CNT subtractor, that number is 30 frames.
        # If the background subtractor hasn't had time to learn the background yet, then it is pointless
        # to use it to infer if a frame should be analyzed or not.
        # Therefore, we return "True" here.
        return True

    # If the previous cases did not yield a positive result, then we proceed with the motion detection.

    kernel = np.ones((5, 5), np.uint8)

    foreground_mask = cv2.erode(foreground_mask, kernel, iterations=1)
    foreground_mask = cv2.dilate(foreground_mask, kernel, iterations=5)

    contours = cv2.findContours(foreground_mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0]

    image_width, image_height = video_frame.shape[0:2]
    minimum_valid_area = (1 / 100) * (image_width * image_height)

    movement_verdict = False

    for contour in contours:
        x, y, w, h = cv2.boundingRect(contour)
        cv2.rectangle(video_frame, (x, y), (x + w, y + h), (255, 255, 0), 2)

        if cv2.contourArea(contour) > minimum_valid_area:
            movement_verdict = True
            break

    return movement_verdict


def main(args):
    if not args.saved_model_path:
        saved_model_path = SAVED_MODEL_DIR.format(args.model_id, args.batch_size)
    else:
        saved_model_path = args.saved_model_path

    assert os.path.isdir(
        saved_model_path
    ), "The saved model for model d{0}, batch size {1} could not be found at path: {2}".format(
        args.model_id, args.batch_size, saved_model_path
    )

    # Load the configuration JSON file for the specified camera.
    camera_config = CameraConfiguration()
    camera_config.load_from_file(args.camera)

    assert os.path.isfile(args.video_input), "the input video path is not a file or does not exist"

    video = cv2.VideoCapture(args.video_input)
    assert video.isOpened(), f"Error opening input video: {args.video_input}"

    # Seek to the requested start frame if any.
    if args.start_frame > 0:
        video.set(cv2.CAP_PROP_POS_FRAMES, args.start_frame)

    width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
    frames_per_second = video.get(cv2.CAP_PROP_FPS)
    num_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
    basename = os.path.basename(args.video_input)

    inference_driver = create_inference_driver(
        model_id=args.model_id,
        saved_model_path=saved_model_path,
        min_score_thresh=args.confidence_threshold,
        batch_size=args.batch_size,
    )
    data_aggregator = FrameDataAggregator()

    background_subtractor = create_background_subtractor(video_fps=frames_per_second)

    # Keep track of the number of people in the last RECENT_FRAMES_PEOPLE_COUNT_HISTORY frames. This will be used by the should_process_video_frame function.
    recent_frames_people_count = collections.deque(maxlen=RECENT_FRAMES_PEOPLE_COUNT_HISTORY)

    # If the user wants the annotations to be saved then we do it here.
    # Otherwise, we just present the annotations in an OpenCV window if the --display flag was passed.
    if args.output:
        # If the specified output is a directory, then we save the file in:
        # {output_directory}/{output_file_name}-dd-mm-yyyy hh_mm_ss.mkv
        # Otherwise, if the specified output is a regular file path then we save the file there.
        if os.path.isdir(args.output):
            output_filename = os.path.join(args.output, basename)
            output_filename = os.path.splitext(output_filename)[0] + "-" + datetime.now().strftime("%d-%m-%Y %H_%M_%S") + ".mkv"
        else:
            output_filename = args.output

        # Make sure the file does not exist already.
        assert not os.path.isfile(output_filename), "The file at path {0} already exists.".format(
            output_filename
        )

        output_file = cv2.VideoWriter(
            filename=output_filename,
            # some installation of opencv may not support x264 (due to its license),
            # you can try other format (e.g. MPEG)
            fourcc=cv2.VideoWriter_fourcc(*"x264"),
            fps=float(frames_per_second),
            frameSize=(width, height),
            isColor=True,
        )

    start = time.perf_counter()

    frames = list()
    dummy_image = np.zeros((height, width, 3), dtype=np.uint8)

    for i, frame in enumerate(
        tqdm.tqdm(
            video_frame_generator(video),
            total=num_frames,
            initial=args.start_frame,
            disable=args.disable_output,
        ),
        start=args.start_frame,
    ):
        # Should we run inference on this frame ?
        is_frame_relevant = should_process_video_frame(
            recent_frames_people_count, background_subtractor, frame, i
        )

        # Add the frame to the batch if it is relevant.
        if is_frame_relevant:
            frames.append(frame)

        if i == num_frames - 1:
            # This is the last frame!

            # If the batch could not be entirely filled up (the video file does not have a multiple of batch_size frames),
            # then we add dummy frames. We must do this because the inference model _must_ be ran with batch_size number of frames. Not fewer.

            # How many dummy frames do we need to add?
            num_missing_frames = args.batch_size - len(frames)
            dummy_frames = [dummy_image for _ in range(0, num_missing_frames)]

            # Add these dummy frames to the batch.
            frames.extend(dummy_frames)

        if len(frames) < args.batch_size:
            # Skip the rest of the loop because the batch is not full yet.
            continue

        # Run inference on this batch of frames.
        detections_ = inference_driver.serve_images(np.array(frames))

        for detections, frame_ in zip(detections_, frames):
            frame_data = FrameData(
                detections, camera_config.homography, args.confidence_threshold, i
            )

            # Append the number of people in this frame to the deque. Because this deque has a maximum of 5 entries,
            # the oldest entry will be dropped automatically when appending this new entry.
            recent_frames_people_count.append(frame_data.number_of_people)

            # Only bother drawing if the user has specified an output file (--output) or a live visualization (--display)
            if args.output or args.display:
                draw_frame_data_on_image(frame_data, frame_)

            data_aggregator.add_frame_data(frame_data)

            if args.output:
                output_file.write(frame_)
            if args.display:
                cv2.namedWindow(basename, cv2.WINDOW_NORMAL)
                cv2.imshow(basename, frame_)
                if cv2.waitKey(1) & 0xFF == ord("q"):
                    return

        # Clear the frames batch (i.e. start a new batch)
        frames = list()

    # Output some simple stats (FPS and SPF)
    end = time.perf_counter()
    spf = (end - start) / (num_frames - args.start_frame)
    fps = 1 / spf
    print(f"Done! SPF: {spf} s/frame or {fps} fps")

    # We processed the entire video file, so we close the VideoCapture,
    # close the output VideoWriter and destroy all OpenCV windows.
    video.release()
    if args.output:
        output_file.release()
    if args.display:
        cv2.destroyAllWindows()

    return fps


def parse_args(args):
    """Parses a list of arguments (e.g. ['--model-id', '0', '--video-input', '/Users/tim/git/ ... /xxx.mp4', '--camera', '/Users/tim/git/ ... /config.JSON'])
       and returns those arguments in the form of argparse.Namespace

    Args:
        args (list): a list of arguments, typically from sys.argv[1:]

    Returns:
        argparse.Namespace: the parsed arguments
    """
    parser = argparse.ArgumentParser(description="Simple inference script for EfficientDet.")
    parser.add_argument("--model-id", help="model id, from 0 to 7", type=int, required=True)
    parser.add_argument(
        "--batch-size", help="inference batch size, defaults to 1.", type=int, default=1
    )

    parser.add_argument(
        "--saved-model-path",
        help="Folder path to saved model If not specified, will look into models/efficientdet-d(i)-sv-batch(y) folder",
    )

    parser.add_argument("--video-input", help="Path to video file.", required=True)
    parser.add_argument(
        "--start-frame",
        help="The frame number to start reading the video file from. Default is 0.",
        type=int,
        default=0,
    )

    parser.add_argument(
        "--camera",
        type=str,
        required=True,
        help="The path to the JSON configuration file for the analyzed camera.",
    )

    parser.add_argument("--output", help="A file or directory to save output visualizations. ")

    parser.add_argument(
        "--display", action="store_true", help="display visualizations in an OpenCV window."
    )

    parser.add_argument(
        "--disable-output", action="store_true", help="disables progress bar output"
    )

    parser.add_argument(
        "--confidence-threshold",
        type=float,
        default=0.4,
        help="Minimum score for instance detections to be shown",
    )

    return parser.parse_args(args)


if __name__ == "__main__":
    args = parse_args(sys.argv[1:])
    main(args)
