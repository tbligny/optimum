import collections
from types import SimpleNamespace

import numpy as np
import sklearn.metrics

from social_distancing.automl.efficientdet.keras import label_util
from social_distancing.camera.homography import project_image_coordinates_to_world_coordinates


class FrameData:
    def __init__(self, detections, homography, confidence_threshold, frame_number):
        """Given a list of detections, a confidence threshold on those detections and the 3x3 homography matrix,
            This calculates a set of useful measurements based on this data.

        Args:
            detections (np.ndarray): the list of detections according to the following format:
                [image_id, y_min, x_min, y_max, x_max, score, class].
            homography (np.ndarray): the 3x3 homography matrix.
            confidence_threshold (float): the minimum score level for detections
                to be considered valid.
            frame_number (int): the frame's index in the current video.
        """
        self.frame_number = frame_number

        # During all our analyses in this class, we are only concerned about people, so we filter out everything else.
        self.detections = filter_detections(
            detections, ["person"], confidence_threshold=confidence_threshold
        )

        # The number of people detected in this frame (with a score above the confidence threshold!)
        self.number_of_people = len(self.detections)

        # Calculate the (x, y) position of people in image coordinates.
        self.positions_image = calculate_people_positions_from_detections(
            self.detections, confidence_threshold, filter_people=False
        )

        # Transfer to world coordinates.
        self.positions_world = project_image_coordinates_to_world_coordinates(
            self.positions_image, homography
        )

        # The mean distance, taking only in consideration the distances under DISTANCE_THRESHOLD.
        self.mean_distance = np.nan

        # The pairwise NxN distance matrix.
        self.distance_matrix = None

        # A numpy vector of relevant pairwise distances, that is, distances that are under DISTANCE_THRESHOLD.
        self.relevant_pairwise_distances = None

        # Calculate the distance matrix if we can.
        if len(self.positions_world) > 1:
            self.distance_matrix = sklearn.metrics.pairwise.euclidean_distances(
                self.positions_world
            )

            # We can calculate the mean distance if we have the distance matrix.

            # Get the indices of the values above the main diagonal (k=1).
            # The diagonal is obviously all zeroes and the matrix is symmetric
            # because we are doing pairwise distances.
            indices = np.triu_indices_from(self.distance_matrix, k=1)

            # Calculate the mean distance between people.
            # To do this, we get an upper triangular matrix (which contains 0 in the lower triangle and main diagonal)
            # From this matrix, we switch any distance above a threshold to an invalid value. Long distances between people
            # should not affect the mean.
            # From this matrix, we apply a mask that switches any "0" values to invalid values which will not be counted
            # by np.mean()
            #
            # Above this value, people are not considered to be close to one another.
            DISTANCE_THRESHOLD = 4
            masked_lower_triangular = np.ma.masked_equal(self.distance_matrix[indices], 0)
            masked_long_distances = np.ma.masked_greater_equal(
                masked_lower_triangular, DISTANCE_THRESHOLD
            )
            self.relevant_pairwise_distances = masked_long_distances.compressed()

            # Make sure there is at least one valid distance before trying to calculate the mean.
            # Otherwise, we cannot calculate the mean distance because it does not make sense.
            if len(self.relevant_pairwise_distances) >= 1:
                self.mean_distance = self.relevant_pairwise_distances.mean()


class FrameDataAggregator:
    """Aggregates data from FrameData over multple frames.
    """

    def __init__(self):
        self.mean_distance_aggregation = SimpleNamespace(data=0, meaningful_frames_count=0)
        self.pairwise_distances_aggregation = SimpleNamespace(
            data=list(), meaningful_frames_count=0
        )

    def add_frame_data(self, frame_data):
        """Aggregates data from a new FrameData.

        Args:
            frame_data (FrameData): the FrameData instance for a frame.
        """

        if not np.isnan(frame_data.mean_distance):
            frames_count = self.mean_distance_aggregation.meaningful_frames_count

            old_mean_distance = self.mean_distance_aggregation.data
            new_data_point = frame_data.mean_distance
            new_mean_distance = (old_mean_distance * (frames_count) + new_data_point) / (
                frames_count + 1
            )

            self.mean_distance_aggregation.data = new_mean_distance
            self.mean_distance_aggregation.meaningful_frames_count += 1

        distances = frame_data.relevant_pairwise_distances
        if distances is not None and distances.size > 0:
            # Add the new distances to the list of all distances.

            frame_info = {
                "id": frame_data.frame_number,
                "dist": distances.tolist(),
            }

            self.pairwise_distances_aggregation.data.append(frame_info)
            self.pairwise_distances_aggregation.meaningful_frames_count += 1


def calculate_people_positions_from_detections(
    detections, confidence_threshold, filter_people=False
):
    """From a list of detections (the output of the model), calculate the
        positions of people in the image. The results are in pixel coordinates.
        The position of the feet is calculated, which is approximated as the
        mid-point of the rectangle base.

    Args:
        detections (np.ndarray): the list of detections according to the following format:
            [image_id, y_min, x_min, y_max, x_max, score, class].
        confidence_threshold (float): the minimum score level for detections
            to be considered valid.
        filter_people (bool, optional): whether or not to filter detections to retain only people. Defaults to False.
    Returns:
        np.ndarray: An array of position coordinates (x, y).
    """

    if filter_people:
        # Filter detections to only keep people above a certain confidence threshold.
        detections = filter_detections(
            detections, ["person"], confidence_threshold=confidence_threshold
        )

    positions = list()
    for detection in detections:
        _, x_min, y_max, x_max = detection[1:5]

        # We use the max y value (not the min) because for images, the origin is at the top left.
        # (not the bottom as we are generally used to).
        feet_position = [(x_max + x_min) / 2, y_max]
        positions.append(feet_position)

    return np.array(positions)


def filter_detections(detections, filter_categories, confidence_threshold=0.0):
    """Filters detections to retain only relevant objects above a certain threshold.

    Args:
        detections (np.ndarray): The list of detections according to the following format:
            [image_id, y_min, x_min, y_max, x_max, score, class].
        filter_categories (list): The list of COCO object categories. These categories will be kept.
            The mapping from string to id will be done using 'efficientdet.keras.label_util.coco'.
        confidence_threshold (float): The minimum score level for detections
            to be considered valid.
    Returns:
        np.ndarray: The filtered detections.
    """

    if filter_categories:
        # coco_id_mapping is a dict mapping from id to string. What we want is a dict mapping
        # from string to id. This "reverses" the dictionary.
        reverse_coco_id_mapping = dict(
            zip(label_util.coco.values(), label_util.coco.keys())
        )

        try:
            filter_ids = [reverse_coco_id_mapping.get(key) for key in filter_categories]
        except KeyError as exception:
            print("Requested category is not a valid COCO category. {0}".format(exception))
            return None

        classes = detections[:, 6]
        mask = np.isin(classes, filter_ids)

        # Filter the detections based on the category.
        detections = detections[mask]

    if confidence_threshold > 0.0:
        # Filter the detections based on the score.
        scores = detections[:, 5]
        detections = detections[np.where(scores > confidence_threshold)]

    return detections
