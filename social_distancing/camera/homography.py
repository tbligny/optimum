import numpy as np
import cv2

"""This module contains several useful functions with regard to homographies
   and perspective transforms (the code actually come from OpenCV for the most part).
"""


def order_points(points):
    """ Given a list of four 2D cartesian points, this method will order them
        in a clockwise order with the top-left point as the starting point.

    Args:
        points (np.ndarray): A numpy array containing four 2D cartesian points.

    Returns:
        np.ndarray: The ordered points.
    """
    input_dtype = points.dtype
    output = np.zeros((4, 2), dtype=input_dtype)

    # Recall that in digital images, the origin is placed at the top-left.

    # The top-left point will have the smallest componant sum, whereas
    # the bottom-right point will have the largest sum.
    points_sum = points.sum(axis=1)
    output[0] = points[np.argmin(points_sum)]
    output[2] = points[np.argmax(points_sum)]

    # Now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference.
    diff = np.diff(points, axis=1)
    output[1] = points[np.argmin(diff)]
    output[3] = points[np.argmax(diff)]

    return output


def calculate_four_point_homography(rect):
    """ Calculates the homography matrix based on four rectangle points.

    Args:
        rect (np.ndarray): The four cartesian coordinates of the rectangle to correct.

    Returns:
        (np.ndarray, np.ndarray): Respectively, the destination points and the homography matrix
                                  to perform that performs the transformation.
    """
    (tl, tr, br, bl) = rect

    # Compute the width of the new image, which will be the
    # maximum distance between bottom-right and bottom-left
    # x-coordiates or the top-right and top-left x-coordinates
    width_bottom = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    width_top = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))

    # Now that we have the dimensions of the new image, construct
    # the set of destination points to obtain a "birds eye view",
    # (i.e. top-down view) of the image, again specifying points
    # in the top-left, top-right, bottom-right, and bottom-left order.

    input_dtype = rect.dtype

    dst = np.array(
        [
            [tl[0], tl[1]],
            [tl[0] + (width_bottom + width_top) / 2, tl[1]],
            [tl[0] + (width_bottom + width_top) / 2, br[1]],
            [tl[0], br[1]],
        ],
        dtype=input_dtype,
    )

    homography = cv2.getPerspectiveTransform(rect, dst)
    return (dst, homography)


def perform_perspective_transform(image, homography):
    """ Performs the perspective transform according to the homography matrix.

    Args:
        image (np.ndarray): The input image
        homography (np.ndarray): The homography matrix to perform that performs the transformation.
    Returns:
        np.ndarray: The rectified image. The output image is of the same size as the input image.
    """
    height, width = image.shape[0:2]
    warped = cv2.warpPerspective(image, homography, (width, height))

    return warped


def project_world_coordinates_to_image_coordinates(world_coordinates, homography, integer=False):
    """Projects points from world coordinates to image coordinates using the 3x3 homography matrix.

    Args:
        world_coordinates (np.ndarray): Nx2 vector of world space points.
        homography (np.ndarray): The 3x3 homography matrix where H is: image_coordinates = H * world_coordinates
        integer (bool): Whether to round the output coordinates to integers.
    Returns:
        np.ndarray: a Nx2 vector of world space points.
    """

    # If the input array is empty, then we just return an empty array.
    if world_coordinates.size == 0:
        return world_coordinates

    # Convert the world points to homogeneous coordinates by appending z = 1 to every point.
    ones = np.ones((world_coordinates.shape[0], 1), dtype=world_coordinates.dtype)
    world_coordinates_homogeneous = np.append(world_coordinates, ones, axis=1)

    # Perform the projection.
    image_coordinates = homography.dot(world_coordinates_homogeneous.T).T

    # Convert back from homogeneous coordinates and keep only the x, y elements.
    # To do this, we divide every row by the last vector componant.
    # 'np.newaxis' adds a new axis so that numpy broadcasting can be done.
    image_coordinates = np.true_divide(
        image_coordinates[:, 0:2], image_coordinates[:, -1, np.newaxis]
    )

    if integer:
        image_coordinates = np.round(image_coordinates)

    return image_coordinates


def project_image_coordinates_to_world_coordinates(image_coordinates, homography, integer=False):
    """Projects points from image coordinates to world coordinates using the 3x3 homography matrix.
    This will actually calculate the inverse of the homography matrix.

    Args:
        image_coordinates (np.ndarray): Nx2 vector of world space points.
        homography (np.ndarray): The 3x3 homography matrix where H is: image_coordinates = H * world_coordinates
        integer (bool): Whether to round the output coordinates to integers.
    Returns:
        np.ndarray: a Nx2 vector of image space points.
    """

    # What we need is the _inverse_ homography in this case.
    inverse_homography = np.linalg.inv(homography)

    return project_world_coordinates_to_image_coordinates(
        image_coordinates, inverse_homography, integer=integer
    )
