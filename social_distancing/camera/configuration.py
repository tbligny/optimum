import json
from dataclasses import dataclass

import numpy as np

class NumpyEncoder(json.JSONEncoder):
    """This class is for converting numpy arrays into JSON."""

    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()

        return json.JSONEncoder.default(self, obj)


@dataclass
class CameraConfiguration:
    """This class is for keeping track of a specific canera's details,
       such as its name or its homography matrix"""

    name: str = None
    genetec_ID: str = None
    homography: np.ndarray = None

    def write_to_file(self, file_path):
        """Writes the config in a JSON file.

        Args:
            file_path (str): The file in which to write.
        """
        data = {"name": self.name, "genetec-ID": self.genetec_ID, "homography": self.homography}

        with open(file_path, "w") as output_file:
            json.dump(data, output_file, ensure_ascii=False, cls=NumpyEncoder)

    def load_from_file(self, file_path):
        """Loads a specific config from a JSON file.

        Args:
            file_path (str): The file to read.
        """
        with open(file_path, "r") as input_file:
            data = json.load(input_file)

            self.name = data.get("name")
            self.genetec_ID = data.get("genetec-ID")
            self.homography = np.asarray(data.get("homography"))
