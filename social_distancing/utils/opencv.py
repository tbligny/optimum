from threading import Thread
from queue import Queue
import cv2


def wait_for_window_closure(window_name):
    """ This function will wait until the window is closed ('x' button) or
        if the user clicks on the escape key.

    Args:
        window_name (str): The window to monitor for closure.
    """
    while True:
        k = cv2.waitKey(100)

        if k == 27:  # esc to quit
            break

        if cv2.getWindowProperty(window_name, 0) < 0:
            break

    cv2.destroyAllWindows()


def resize_image(image, scale_factor):
    """Resizes an image according to an scale factor, e.g. 1/5

    Args:
        image (np.ndarray): The image to be resized
        scale_factor (float): The scale factor

    Returns:
        np.ndarray: The resized image.
    """
    width = int(image.shape[1] * scale_factor)
    height = int(image.shape[0] * scale_factor)
    dim = (width, height)

    return cv2.resize(image, dim, interpolation=cv2.INTER_AREA)


def video_frame_generator(video):
    """
    Generates (yields) video frames from a video file.
    Args:
        video (cv2.VideoCapture): a :class:`VideoCapture` object, whose source can be
            either a webcam or a video file.
    Yields:
        np.ndarray: the next frame.
    """
    while video.isOpened():
        success, frame = video.read()

        if success:
            yield frame
        else:
            break


def create_background_subtractor(video_fps):
    """Creates and configures the background subtractor instance.

    Args:
        video_fps (float): the FPS of the video. This value will be used to configure the background subtractor.

    Returns:
        cv2.bgsegm: the configured background subtractor instance.
    """

    background_subtractor = cv2.bgsegm.createBackgroundSubtractorCNT()

    # Configure the background subtractor.
    # For insight into the following two functions calls, see https://sagi-z.github.io/BackgroundSubtractorCNT/doxygen/html/index.html
    #
    # This determines how many frames we should wait before classifying a pixel as background.
    background_subtractor.setMinPixelStability(2 * int(round(video_fps)))
    # This determines for how many frames we should wait before recognizing the background has changed.
    background_subtractor.setMinPixelStability(4 * int(round(video_fps)))

    return background_subtractor


class ThreadedReader(Thread):
    """A threaded wrapper around cv2.VideoCapture which reads frames in memory.
       This is helpful in IO-bound programs, but not so much in CPU-bound programs because of the Python GIL.

       The API here mimics the cv2.VideoCapture API so that it can be used as a drop-in replacement.
    """

    def __init__(self, source=None, max_frames_in_memory=8):
        super(ThreadedReader, self).__init__()
        self.stream = cv2.VideoCapture(source)

        # initialize the variable used to indicate if the thread should be stopped
        self.stopped = False

        # A FIFO queue for the frames that have been read.
        self.frames = Queue(maxsize=max_frames_in_memory)

        self.start()

    def run(self):
        while True:
            # if the thread indicator variable is set, stop the thread
            if self.stopped:
                self.stream.release()
                break

            # otherwise, read the next frame from the stream
            success, frame = self.stream.read()

            # We have reached the end of the stream.
            if not success:
                break

            # This will block until there is space in the queue.
            self.frames.put(frame)

    def read(self):
        # Mimic the VideoCapture API.

        if self.stopped:
            # print("stopped")
            return (False, None)
        else:
            # This will block until a frame is available.
            return (True, self.frames.get())

    def get(self, param):
        return self.stream.get(param)

    def set(self, param, value):
        return self.stream.set(param, value)

    def release(self):
        self.stopped = True

    def isOpened(self):
        return not self.stopped
