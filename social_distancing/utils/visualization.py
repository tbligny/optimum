import cv2
import numpy as np
import PIL.Image as Image
import PIL.ImageDraw as ImageDraw
import PIL.ImageFont as ImageFont

# Au format RGBa
RED = (0, 0, 255, 255)
GREEN = (0, 255, 0, 255)
BLUE = (255, 0, 0, 255)
WHITE = (255, 255, 255, 255)


def image_from_numpy_array(numpy_array):
    """Returns a PIL image from a numpy array.

    Args:
        numpy_array (np.ndarray): the RGB image as a 3D numpy array.
    Returns:
        PIL.Image: the image as an instance of PIL.image
    """
    return Image.fromarray(np.uint8(numpy_array)).convert("RGB")


def image_to_numpy_array(image_PIL, numpy_array):
    """Writes a PIL image to a numpy array.

    Args:
        image_PIL (PIL.Image): the PIL image
        numpy_array ([type]): the destination numpy array
    """

    np.copyto(numpy_array, np.array(image_PIL))


def draw_bounding_box_on_image(image_draw, coordinates, thickness=4, color=GREEN):
    """Draws a bounding box on an image.

    Args:
        image_draw (ImageDraw.Draw): the draw instance to draw with.
        coordinates (tuple): the coordinates of the bounding box: y_min, x_min, y_max, x_max
        thickness (int, optional): thickness of the line. Defaults to 4.
        color (str, optional): color used when drawing. Defaults to GREEN.
    """
    y_min, x_min, y_max, x_max = coordinates

    image_draw.line(
        [(x_min, y_min), (x_min, y_max), (x_max, y_max), (x_max, y_min), (x_min, y_min),],
        width=thickness,
        fill=color,
    )


def draw_text_on_image(
    image_draw, text, position, color=GREEN, draw_background=False, transparency=255
):
    """Draws text on an image

    Args:
        image_draw (ImageDraw.Draw): the draw instance to draw with.
        text (str): the text to write.
        position (tuple): the top left position of the text.
        color (str, optional): color used when drawing. Defaults to GREEN.
        draw_background (bool, optional): whether to draw a black background behind the text. Defaults to False.
        transparency (int, optional): alpha value for transparency. 255 = opaque, 0 = trnsparent. Defaults to opaque (255)
    """
    try:
        image_height = image_draw.im.size[1]
        # Make the font size equal to 5 percent of the image height.
        points = int((0.05 * image_height) * 72 / 96)

        font = ImageFont.truetype("arial.ttf", points)
    except IOError:
        font = ImageFont.load_default()

    x, y = position
    text_width, text_height = font.getsize(text)
    margin = np.ceil(0.05 * text_height)

    if draw_background:
        image_draw.rectangle(
            [(x, y - margin), (x + text_width + 2 * margin, y + text_height + 2 * margin),],
            fill=(0, 0, 0, transparency),
        )

    image_draw.text((x + margin, y + margin), text, fill=color, font=font)


def draw_line_on_image(image_draw, start_point, end_point, thickness=4, color=GREEN):
    """Draws a single line on an image.

    Args:
        image_draw (ImageDraw.Draw): the draw instance to draw with.
        start_point (np.ndarray): a numpy array of the start point.
        end_point (np.ndarray): a numpy array of the end point.
        thickness (int, optional): thickness of the line in pixels. Defaults to 4.
        color (str, optional): color used when drawing. Defaults to GREEN.
    """

    image_draw.line(
        np.array([start_point, end_point]).ravel().tolist(), fill=color, width=thickness
    )


def draw_frame_data_on_image(frame_data, image):
    """Visualizes the computed frame data on an image.

    Args:
        image (np.ndarray): The image to draw on. This array is modified in place.
    """

    # Draw a green dot at each person's position.
    for i in range(0, frame_data.positions_image.shape[0]):
        cv2.circle(
            image,
            tuple(np.int32(np.round(frame_data.positions_image[i]))),
            7,
            (0, 250, 0),
            -1,
            lineType=cv2.LINE_AA,
        )

    # Convert the numpy image to a PIL image for drawing.
    image_PIL = image_from_numpy_array(image)
    image_draw = ImageDraw.Draw(image_PIL, "RGBA")

    # Write the mean distance at the top left of the image.
    if not np.isnan(frame_data.mean_distance):
        text = "Distance moyenne: {:.1f}m".format(frame_data.mean_distance)

        # Draw the text in red if the distance is less than 2 meters.
        color = RED if frame_data.mean_distance <= 2 else GREEN
    else:
        text = "Distance moyenne: "
        color = WHITE

    draw_text_on_image(
        image_draw, text, (0, 0), color=color, draw_background=True, transparency=127,
    )

    # Do we have more than 1 person? If not, there is nothing else we can draw.
    if len(frame_data.positions_world) <= 1:
        image_to_numpy_array(image_PIL, image)
        return

    # Get the indices of the values above the main diagonal (k=1).
    # The diagonal is obviously all zeroes and the matrix is symmetric
    # because we are doing pairwise distances.
    indices = np.triu_indices_from(frame_data.distance_matrix, k=1)

    # Loop through each unique distance.
    for position in zip(*indices):
        row, col = position
        distance = frame_data.distance_matrix[row, col]

        if distance > 2:
            # If the distance between two people is larger than 2 meters, we do not display anything.
            # We only display bounding boxes, lines and text if the distance is less than 2 meters.
            continue

        # The feet positions of the two people.
        start_point = frame_data.positions_image[row]
        end_point = frame_data.positions_image[col]

        # Draw a line between the two people
        draw_line_on_image(image_draw, start_point, end_point, color=RED)

        # Display the distance between the two people.
        mid_point = (
            1.10 * ((start_point[0] + end_point[0]) / 2),
            (start_point[1] + end_point[1]) / 2,
        )
        draw_text_on_image(
            image_draw,
            "{:.1f}m".format(distance),
            mid_point,
            color=RED,
            draw_background=True,
            transparency=127,
        )

        # Draw a red bounding box around the two people.
        draw_bounding_box_on_image(image_draw, frame_data.detections[row][1:5], color=RED)
        draw_bounding_box_on_image(image_draw, frame_data.detections[col][1:5], color=RED)

    # After drawing using PIL, copy augmented the image back to the numpy array.
    image_to_numpy_array(image_PIL, image)
