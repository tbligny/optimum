from itertools import chain, islice, takewhile, repeat, zip_longest
import os

import cProfile
import datetime
import pytz
from functools import wraps
from time import perf_counter


def timing(function):
    """Decorator to time a function
    """

    @wraps(function)
    def wrap(*args, **kw):
        time_start = perf_counter()
        result = function(*args, **kw)
        time_end = perf_counter()
        print("func:%r took: %2.4f sec" % (function.__name__, time_end - time_start))
        return result

    return wrap


def profileit(func):
    """Decorator to profile a function

    Args:
        func : the function to profile. Output will be saved as <func.__name__>-<date>."profile"
    """

    def wrapper(*args, **kwargs):
        prof = cProfile.Profile()

        retval = prof.runcall(func, *args, **kwargs)

        output_file = (
            func.__name__
            + "-{date:%Y-%m-%d_%H-%M-%S}".format(date=datetime.datetime.now())
            + ".profile"
        )
        prof.dump_stats(output_file)

        print(f"Wrote profiling output to: {os.path.abspath(output_file)}")

        return retval

    return wrapper


def string_to_ISO8601_date(datestr):
    """Converts a string in ISO 8601 format to a datetime object.

    Args:
        datestr (str): the date in ISO 8601 format.

    Returns:
        datetime: the same date as a datetime object.
    """
    return datetime.datetime.strptime(datestr, "%Y-%m-%dT%H:%M:%S%z")


def date_to_ISO8601_string(date):
    """Converts a datetime object to ISO 8601 format.

    Args:
        date (datetime): the datetime to convert

    Returns:
        str: the datetime object as a string in the ISO8601 format.
    """
    if date:
        timezone = pytz.timezone("US/Eastern")
        # .replace(microsecond=0) removes the microsecond component which is not relevant to us.
        return date.astimezone(timezone).replace(microsecond=0).isoformat()
    else:
        return None
