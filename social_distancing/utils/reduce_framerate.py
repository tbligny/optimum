import argparse
import cv2
import tqdm

# Côte-Vertu video file is 7.001087547580207 fps


def frame_generator(video):
    while video.isOpened():
        success, frame = video.read()
        if success:
            yield frame
        else:
            break


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i", "--input", dest="input_video_path", help="path to the input video", required=True,
    )
    parser.add_argument(
        "-o", "--output", dest="output_video_path", help="path to the output video", required=True,
    )
    parser.add_argument(
        "-f",
        "--frame-rate",
        dest="desired_frame_rate",
        help="desired video FPS",
        type=float,
        required=True,
    )

    args = parser.parse_args()

    video = cv2.VideoCapture(args.input_video_path)
    if not video.isOpened():
        print("Error opening input video: {0}".format(args.input_video_path))
        return

    width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
    num_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
    frames_per_second = video.get(cv2.CAP_PROP_FPS)

    print(frames_per_second)
    output_file = cv2.VideoWriter(
        filename=args.output_video_path,
        fourcc=cv2.VideoWriter_fourcc(*"x264"),
        fps=args.desired_frame_rate,
        frameSize=(width, height),
        isColor=True,
    )

    for frame in tqdm.tqdm(frame_generator(video), total=num_frames):
        output_file.write(frame)


if __name__ == "__main__":
    main()
